
#%%
#!/usr/bin/env python
import argparse
from math import sin, cos, sqrt
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import numpy as np

def parse_args():
    """Function takes arguments from terminal in Unix style."""
    parser = argparse.ArgumentParser(description="Program of wrapper pendulum")
    parser.add_argument("-Q", type=float, help="Q")
    parser.add_argument("-w", type=float, help="Omega")
    parser.add_argument("-A", type=float, help="A")
    parser.add_argument("-v", type=float, help="v_0")
    parser.add_argument("-th",type=float, help="th_0")

    args = parser.parse_args()
    return args

def solving_equation(y,tau,Q,w,A):
    """Here 'y' is a vector such that x=y[0] and v=y[1]. This function should return [x', v']"""
    x=y[0]
    v=y[1]
    d_v= (-1/Q)*v - sin(x) + A*cos(w*tau)
    return [v, d_v]
    
if __name__ =="__main__":
    #create tau
    time= np.linspace(0, 100, 1000)   
    g=10 # m/s^2
    l=1 # m
    tau= sqrt(g/1)*time

    args=parse_args()
    dane=[args.th, args.v]
    y0=odeint(solving_equation, dane, time, args=(args.Q,args.w,args.A))
  
    fig, axes= plt.subplots(nrows=1, ncols=2, figsize=(9, 4.5))
    axes[0].plot(time, y0[:,0], color="purple")
    axes[0].set_title('theta-time graph')
    axes[0].set_xlabel("time")
    axes[0].set_ylabel("theta")
    axes[0].grid(True,linestyle='dashed')

    axes[1].plot(y0[:,0], y0[:,1], color="orange")
    axes[1].set_title('velocity-position graph')
    axes[1].set_xlabel("position")
    axes[1].set_ylabel("velocity")
    axes[1].grid(True,linestyle='dashed')
    fig.tight_layout()
    plt.show()


 #wywaołanie python oscylator_wrapper.py -Q 2 -w 0.66 -A 0.5 -v 0 -th 0.01



# %%
