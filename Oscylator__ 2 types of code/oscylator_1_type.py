#%%

#!/usr/bin/env python

from math import sin, cos, sqrt
import matplotlib.pyplot
from scipy.integrate import odeint
import numpy as np
import sys

def parse_input(args):
    ''' Function takes arguments from terminal and puts them into a list. First one (index 0) is name of file'''
    try:
        Q=float(sys.argv[1])
        OMEGA= float(sys.argv[2])
        A= float(sys.argv[3])
        V_0=  float(sys.argv[4])
        THETA_0= float(sys.argv[5])

        if len(sys.argv)!=6:
            print ("Do not give exact 6 arguments")  
    except ValueError:
        print("Error",file=sys.stderr)
        sys.exit(1)
    return Q, OMEGA,A,V_0,THETA_0


def solving_equation(y,tau,Q,OMEGA,A):
    """Here 'y' is a vector such that x=y[0] and v=y[1]. This function should return [x', v']"""
    x=y[0]
    v=y[1]
    d_v=(-1.0/Q)*v - sin(x) + A*cos(OMEGA*tau)
    return [v, d_v]

if __name__=="__main__":

    time = np.linspace(0, 100, 1000) 
    g=10 #m/s^2
    l=1 # m
    tau= sqrt(g/1)*time

    Q,OMEGA,A,V_0,THETA_0= parse_input(sys.argv)
    dane=[THETA_0,V_0]
    y0=odeint(solving_equation, dane, time , args=(Q,OMEGA,A))

    fig, axes= plt.subplots(nrows=1, ncols=2, figsize=(9, 4.5))
    axes[0].plot(time, y0[:,0], color="purple")
    axes[0].set_title('theta-time graph')
    axes[0].set_xlabel("time")
    axes[0].set_ylabel("theta")
    axes[0].grid(True,linestyle='dashed')

    axes[1].plot(y0[:,0], y0[:,1], color="orange")
    axes[1].set_title('velocity-position graph')
    axes[1].set_xlabel("position")
    axes[1].set_ylabel("velocity")
    axes[1].grid(True,linestyle='dashed')
    fig.tight_layout()
    plt.show()

# wywołanie python oscylator.py 2 0.66 0.5 0 0.01

# %%
