#!/usr/bin/env python
import sys # używmay aby zamknąć program
import pandas as pd #używamy do stworzenia tabelki z danymi kursów
from urllib.request import urlopen #aby pobrać linka
from xml.etree.ElementTree import parse # aby pobrać dane z XML i sparsować je
import tkinter as tk # używamu do stwotzenia GUI
from tkinter import ttk # --||--

def quit():
    sys.exit()

def bank():
    """Funckja, która znajduje wszytskie kody walut, kursy, przeliczniki ze strony 'www.nbp.pl', podanej w url i zwraca listy. 
    W liście 's' musimy w każdym elemencie zamienić przecinek na kropkę, żeby móc potem użyć ich do obliczeń. 
    Zpisujemy je w nowej liście 'nowa'. Trzy listy: c, p, nowa tworzą tabelkę wymiany """

    url=urlopen("https://www.nbp.pl/kursy/xml/LastA.xml")
    doc=parse(url)

    c=[]
    s=[]
    p=[]
    
    for item in doc.iterfind("pozycja"):
        code=item.findtext("kod_waluty")
        mm=item.findtext("kurs_sredni")
        xx=item.findtext("przelicznik")
        c.append(code)
        s.append(mm)
        p.append(xx)
        
    nowa=[]
    for i in s:
        z=i.replace(",",".")
        nowa.append(z)

    return c,p,nowa

dicV={"Kod":bank()[0],"Kurs":bank()[2], "Przelicznik":bank()[1]}
wymiana= pd.DataFrame(dicV)
wymiana.to_csv("tabela_kursów.csv")
wymiana

def przeliczamy_na():
    """ Funkcja przelicza wartość wpisną( kwota=entry.get()) z jednej waluty na drugą (variable2.get(),variable1.get()) i
    zwraca jej wartość w innej walucie, która jest typem float. 
    Funckja bierze kursy obu walut z tabeli 'wymiana' oraz ich przeliczniki."""
    
    kwota=entry.get()
    kod_obecny=variable.get()
    kod_pożądany=variable1.get()
    
    z=list(wymiana["Kod"])

    f=z.index(kod_obecny)
    e=z.index(kod_pożądany)

    #bierzemy kurs1 i przeliczniki1 waluty wprowadzanje
    kurs=float(wymiana["Kurs"][f])
    przelicznik=float(wymiana["Przelicznik"][f])

    #bierzemy kurs2 i przelicznik2 waluty uzyskanej
    kurs2=float(wymiana["Kurs"][e])
    przelicznik2=float(wymiana["Przelicznik"][e])

    #obliczamy kwotą i zaokrąglamy ją do 4 miejsc po przecinku
    money=(float(kwota)/przelicznik)*kurs/(kurs2*przelicznik2)
    money1=round(money,4)

    #wpisujemy uzyskaną kwotę w stworzone miejsca do wpisywania --->enrty1
    entry1.insert(0,str(money1))
    

def clear_all() :
    """Funkcja która kasuje zawartość okna w GUI"""
    entry1.delete(0, END)

if __name__=="__main__":

    print( clear_all())
    print(bank())
    print(przeliczamy_na())
    print(quit())

    dicV={"Kod":bank()[0],"Kurs":bank()[2], "Przelicznik":bank()[1]}
    wymiana= pd.DataFrame(dicV)
    wymiana.to_csv("tabela_kursów.csv")
    wymiana
       
    #tworzymy okno
    root=tk.Tk()
    #nadajemy tytuł,
    root.title("Wymiana walut")
    #kolor tła,
    root.configure(bg='CadetBlue')
    #rozmiary
    root.geometry('425x425')
    #nie można zmieniać rozmiaru okna w kazdym wymiarze
    root.resizable(False,False)

    #tytuł 
    heading=tk.Label(root, text="Wymiana walut", font=("arial", 16))
    heading.place(relx=0.40, rely=0.03)

    #tworzymy ramki
    frame=tk.Frame(root, bg='LightBlue', bd=10)
    frame.place(relx=0.25, rely=0.25, relwidth=0.3, relheight=0.1, anchor='n')

    frame1=tk.Frame(root, bg='LightBlue', bd=10)
    frame1.place(relx=0.35, rely=0.50, relwidth=0.5, relheight=0.1, anchor='n')

    #tworzymy miejsca do wpisywania i wyświetlania
    entry = tk.Entry(frame)
    entry.place(relwidth=1, relheight=1.5)

    entry1 = tk.Entry(frame1,textvariable=przeliczamy_na)
    entry1.place(relwidth=1, relheight=1.5)

    #pomocne teksty
    count = tk.Label(root,text='Kwota wprowadzana: ',font=("ariel", 8))
    count.place(relx=0.1, rely=0.18)

    count11 = tk.Label(root,text='Kwota uzyskana: ',font=("ariel", 8))
    count11.place(relx=0.1, rely=0.43)

    fromm = tk.Label(root,text='Z: ',font=("ariel", 8))
    fromm.place(relx=0.42, rely=0.18)

    on = tk.Label(root,text='Na: ',font=("ariel", 8))
    on.place(relx=0.62, rely=0.18)

    #przycisk uruchamiający kasację okna
    clear=tk.Button(root, text="wyczyść kwtotę uzyskaną",bg ='LightBlue',fg = "black", font=("ariel", 10), command = clear_all)
    clear.place(relx=0.1, rely=0.62 ,relwidth=0.40, relheight=0.07)

    #przycisk uruchamiający obliczanie
    button = tk.Button(root, text="oblicz", font=20, command=przeliczamy_na)
    button.place(relx=0.8, rely=0.25, relwidth=0.18, relheight=0.1)

    #przycisk zamykjący okno
    exit_button = tk.Button(root, text="zakończ", font=18, command= quit)
    exit_button.place(relx=0.8, rely=0.5, relwidth=0.18, relheight=0.1)
    
    kod_waluty=bank()[0] # funkcja bank bank()[0] zwraca listę pierwszą z kodami walut

    #drop down box
    variable = tk.StringVar(root)
    variable.set("wybierz")

    variable1 = tk.StringVar(root)
    variable1.set("wybierz")

    #wyświetla pierwszą listę z kodami walut
    drop=ttk.Combobox(root, textvariable=variable, values=kod_waluty )
    drop.place(relx=0.42, rely=0.25, relwidth=0.15, relheight=0.1)

    #wyświetla pierwszą listę z kodami walut
    drop1=ttk.Combobox(root, textvariable=variable1, values=kod_waluty )
    drop1.place(relx=0.62, rely=0.25, relwidth=0.15, relheight=0.1)

    #wyświetla okno
    root.mainloop()

input()


