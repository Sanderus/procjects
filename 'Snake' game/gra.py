import pygame
import random
import sys
import time
import tkinter as tk
from PIL import Image, ImageTk

def quit():
    """ Wychodzenie z programu(gry)."""
    sys.exit()

def ins():
    """ Zamienianie tekstu w Label 'lo' króry jest na obecny."""

    text="Wciel się w węża sterując \n nim strzałkami klawiatury, \n zjadaj jabłka i zbieraj punkty! \n Uwaga! Za każde uderzenie w żywopłot\n Twój wąż straci jedno serduszko.\n Gra zakończy się w momencie utraty \n wszystkich żyć. \n Jak długiego węża dasz radę stworzyć?"
    lo.config(text=text,font=("Courier New",14,'bold'))
    lo.place(x=270,y=240)


def au():
    """ Zamienianie tekstu w Label 'lo' króry jest obecny."""

    text="Cześć! Nazywam się Sandra Czajkowska \n i jestem studentką pierwszego\n roku Matematyki Stosowanej \n na Politechnice Wrocławskiej. \n Jestem autorką kodu niniejszej gry i \n życzę powodzenia :) \n\n adres email: sanderus@interia.pl"
    lo.config(text=text,font=("Courier New",14,'bold'))
    lo.place(x=270,y=240)

def wynik():
    """ Zamienianie tekstu wraz z danymi z otwatego pliku. """

    file = open("best_score.txt", 'r')
    nums = file.read()
    text="Najlepszy wynik to "
    lo.config(text=text+nums,font=("Courier New",18,'bold'))
    lo.place(x=330,y=240)

def game():
    """Wraz ze wszystkim zmiennymi, ikonkami, dźwiękami oraz pomiejszymi funckcjami tworzona jest gra Snake. """

    pygame.init()
    width=670
    height =620
    title="Snake"
    screen=pygame.display.set_mode((width, height))
    best_score="best_score.txt"

    #tytuł i ikonka
    pygame.display.set_caption(title)
    icon = pygame.image.load('snake (2).png')  #ikonka w okienku głónym
    pygame.display.set_icon(icon)

    #kolory tła
    Green_body=(123, 179, 46)
    Green_back=(52,136,60)

    #pozycja początkująca węża
    x=330
    y=300
    x_change = 0
    y_change = 0
    player_pos = [x,y]
    snake_head= pygame.image.load("snake (3).png") 

    lives=3

    def snake(lista):
        """ Funckja pobiera listę 'lista' i na jej podstawie podaje położenie głowy węża i reszty ciała."""

        if direction == "left":
            head = pygame.transform.rotate(snake_head, 270)
            for XnY in lista[:-1]:
                pygame.draw.circle(screen, Green_body, (XnY[0]+82,XnY[1]+37),12)

        if direction == "right":
            head = pygame.transform.rotate(snake_head, 90)
            for XnY in lista[:-1]:
                pygame.draw.circle(screen, Green_body, (XnY[0]-15,XnY[1]+30),13)

        if direction == "down":
            head = snake_head
            for XnY in lista[:-1]:
                pygame.draw.circle(screen, Green_body, (XnY[0]+33,XnY[1]-20),13)

        if direction == "up":
            head = pygame.transform.rotate(snake_head, 180)
            for XnY in lista[:-1]:
                pygame.draw.circle(screen, Green_body, (XnY[0]+30,XnY[1]+80),13)
        
        screen.blit(head, (lista[-1][0], lista[-1][1]))
    
    def collision_with_self(snake_C): # funckja do sprawdzania czy głowa węża najechała na ogon
        snake_he = snake_C[0]
        if snake_he in snake_c[1:]:
            return 1
        else:
            return 0

    #puchar
    puchar= pygame.image.load("cup.png")

    #życia
    serce=pygame.image.load("heart.png") 

    #ogrodzenie
    krzak=pygame.image.load("plant (1).png") 

    #trawa
    trawa=pygame.image.load("grass.png") 

    #font
    myfont = pygame.font.SysFont("monospace", 16)
    myfont1 = pygame.font.SysFont("monospace", 26)
        
    #pozycja jabłka
    apple_pos=[random.randint(70,590),random.randint(120,540)]
    apple= pygame.image.load("nature.png") 

    def draw_lives(x, y, lives, img):
        """ Funckja która bierze położenie 'x' i 'y' gdzie mają się znajdować obiekty. 
        'Lives' jako ilość która ma być narysowana oraz 'img' to obrazek do załadowania."""

        for i in range(lives):
            x_l= x + 50 * i
            y_l= y
            screen.blit(img, (x_l,y_l))


    def draw_bush(x, y, ilość, img, kąt):
        """ Funckja która bierze położenie 'x' i 'y' gdzie mają się znajdować obiekty. 
        'ilość' jako ilość która ma być narysowana, 'img' to obrazek do załadowania oraz 'kąt' o jaki ma być obrócnony obrazek."""

        for i in range(ilość):
            krzak = pygame.transform.rotate(img,kąt)
            x_l= x 
            y_l= y + 70*i
            screen.blit(krzak, (x_l,y_l))

    def draw_bush_x(x, y, ilość, img,kąt):
        """ Funckja która bierze położenie 'x' i 'y' gdzie mają się znajdować obiekty. 
        'ilość' jako ilość która ma być narysowana, 'img' to obrazek do załadowania oraz 'kąt' o jaki ma być obrócnony obrazek."""

        for i in range(ilość):
            krzak = pygame.transform.rotate(img,kąt)
            x_l= x + 70*i
            y_l= y 
            screen.blit(krzak, (x_l,y_l))

    SPEED =0
    #prędkość
    def speed(score, SPEED):
        """Funckja pobiera punkty i prędkość i zwraca prędkość w zależności od ilośći punktów"""

        if score < 12:
            SPEED += 3
        elif score < 20:
            SPEED += 5
            
        elif score < 30:
            SPEED += 9
        else:
            SPEED += 11
        return SPEED

    #kolizja
    snake_size=20
    apple_size=18
    def collision(Head,apple_pos):
        """" Kolizaj węża z jabłkiem. Bierzemy listę 'Head' gdzie są tam zapisane współrzędne głowy węża oraz listę 'apple_pos' 
        gdzie są współrzędne jabłka. Funkcja zwraca 'True' jeśli głowa węża najechała na jabłko w przeciwnym razie zwraca 'False'."""

        x_a=apple_pos[0]
        y_a=apple_pos[1]

        x=Head[0]
        y=Head[1]

        if (x_a >= x and x_a < (x + snake_size)) or (x >= x_a and x < (x_a +apple_size)):
            if (y_a >= y and y_a < (y + snake_size)) or (y >= y_a and y < (y_a + apple_size)):
                return True
        return False




    #punkty
    score=0
    game=True

    clock = pygame.time.Clock()
    direction = 'down' #domyślne ustawienie głowy węża
    snakeLength = 1
    snake_C=[]

    #otworzenie listy
    lista=[]
    file = open(best_score, 'r')
    nums = file.readlines()
    nums = [int(i) for i in nums]
    dodaj=nums[0]
    lista.append(dodaj)
    okej=max(lista)
    

    # Game Loop
    running = True

    while running:

        while game==False:
            screen.fill(Green_back)
            koniec=myfont1.render("Koniec gry. Twoje punkty to "+str(score)+".", 1, (0,0,0))
            screen.blit(koniec,(50,150))
            pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    direction = 'left'
                    x_change = -13
                    
                elif event.key == pygame.K_RIGHT:
                    direction = 'right'
                    x_change = 13
                    
                elif event.key == pygame.K_UP:
                    direction = 'up'
                    y_change = -13
                
                elif event.key == pygame.K_DOWN:
                    direction = 'down'
                    y_change = 13
                
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    y_change = 0

        #kolejne położenia
        x += x_change 
        y += y_change
    
        #jeżeli wąż wejdzie w ogrodzenie to traci życie
        if x > 595 or x <50 or y<80 or y> 545:
            if lives==3 or lives==2 or lives==1:
                #score=0
                x=330
                y=300
                lives-=1
            elif lives==0:
                game=False
        
        screen.fill((166,218,149))

        screen.blit(apple, (apple_pos[0], apple_pos[1]))
        
        draw_lives(520,10,lives,serce) # rysowanie serc jako życia
        draw_bush(0,100,7,krzak,90) # rysowanie krzaków
        draw_bush(610,100,7,krzak,270)
        draw_bush_x(60,62,8,krzak,0)
        draw_bush_x(60,560,8,krzak,180)

        screen.blit(trawa,(415,216))    # ikonki traw musiały być tak zapisane, jeżeli byly zapisane w funckji z ranom.randeint 
        screen.blit(trawa,(215,266))    # to w każdej sekundzie "tańczyły" zmineiąły położenie
        screen.blit(trawa,(288,506))
        screen.blit(trawa,(565,376))
        screen.blit(trawa,(505,186))
        screen.blit(trawa,(195,312))
        screen.blit(trawa,(381,476))
        screen.blit(trawa,(400,266))
        screen.blit(trawa,(90,96))
        screen.blit(trawa,(100,322))

        Head = []
        Head.append(x)
        Head.append(y)
        snake_C.append(Head)
        

        
        if len(snake_C)> snakeLength:
            del snake_C[0]

        snake(snake_C)

        #collision_with_self(snake_C)  jeżeli wąż najedzie na swój ogon to koniec gry

        #zliczanie żyć
        lista_score=[]
        lista_score.append(score)

        score_dis = myfont.render("Score: "+str(score), 1, (0,0,0))
        screen.blit(score_dis, (5, 10))

        liczba=max(lista_score)
        if liczba>okej:
            okej=liczba
        else: 
            okej

        # zapisywanie nowego najlepszego wyniku 
        g=open(best_score,"w")
        g.write(str(okej))
        g.close()

        best= myfont.render("Best: "+str(okej), 1, (0,0,0))
        screen.blit(best, (150, 10))
        screen.blit(puchar, (110,10))

        pygame.display.update()


        # kolizja
        collision1 = collision(Head, apple_pos)
        if collision1:
            sound = pygame.mixer.Sound("apple.wav")# plik 
            sound.play()
            score += 1

            snakeLength+=1

            #nowa pozycja jabłka
            apple_pos[0] = random.randint(70,590)
            apple_pos[1] = random.randint(120,530)

        
            SPEED=speed(score,SPEED)
            
        clock.tick(20+SPEED)
    pygame.quit()

root=tk.Tk()
root.geometry('700x580')
root.resizable(False,False)
root.title("Snake")
root.config(background='forest green')

x_lab=tk.Label(root,padx=12,pady=12,bd=4,fg="black",bg='forest green',text="Gra Snake",font=("Courier New",22,'bold'))
x_lab.place(x=80, y=50)


start_gry=tk.Button(root,padx=12,pady=12,bd=4,fg="black",bg='red4',command=game,text="Start gry",font=("Courier New",16,'bold'))
start_gry.place(x=80,y=160)

instrukcja=tk.Button(root,padx=12,pady=12,bd=4,fg="black",bg='firebrick3',command=ins,text="Zasady gry",font=("Courier New",16,'bold'))
instrukcja.place(x=80,y=230)

wynik=tk.Button(root,padx=12,pady=12,bd=4,fg="black",bg='firebrick3',command=wynik,text="Najlepszy \n wynik",font=("Courier New",14,'bold'))
wynik.place(x=80,y=370)

autor=tk.Button(root,padx=12,pady=12,bd=4,fg="black",bg='firebrick3',command=au,text="O autorze",font=("Courier New",16,'bold'))
autor.place(x=80,y=300)

wyjdz=tk.Button(root,padx=12,pady=12,bd=4,fg="black",bg='firebrick3',command=quit,text="Wyjdź",font=("Courier New",16,'bold'))
wyjdz.place(x=80,y=463)


lo=tk.Label(root,fg="black",bg='forest green',text="Zagraj i \nbądź najlpeszy w Snake!!!",font=("Courier New",18,'bold'))
lo.place(x=320,y=240)

image = Image.open("snake (3).png")
photo = ImageTk.PhotoImage(image)
label = tk.Label(image=photo,bg='forest green')
label.image = photo # keep a reference!
label.place(x=270,y=50)

root.mainloop()


game()
input()


